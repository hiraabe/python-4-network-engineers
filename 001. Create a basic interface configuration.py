#!/usr/bin/python3

import getpass
import telnetlib

HOST = "192.168.122.85"
user = input("Enter your telnet username: ")
password = getpass.getpass()

tn = telnetlib.Telnet(HOST)

tn.read_until(b"Username: ")
tn.write(user.encode('ascii') + b"\n")
if password:
    tn.read_until(b"Password: ")
    tn.write(password.encode('ascii') + b"\n")

tn.write(b"enable\n")
tn.write(b"Pa$$word\n")
tn.write(b"conf t\n")
tn.write(b"int loop 0\n")
tn.write(b"ip address 1.1.0.1 255.255.255.255\n")
tn.write(b"exit\n")
tn.write(b"int loop 1\n")
tn.write(b"ip address 1.1.1.1 255.255.255.255\n")
tn.write(b"exit\n")
tn.write(b"int loop 3\n")
tn.write(b"ip address 1.1.2.1 255.255.255.255\n")
tn.write(b"exit\n")
tn.write(b"int gig0/1\n")
tn.write(b"Description **To ASW-MOG **\n")
tn.write(b"ip address 192.168.10.254 255.255.255.0\n")
tn.write(b"no shut\n")
tn.write(b"exit\n")
tn.write(b"int gig0/2\n")
tn.write(b"Description **To ASW-HRG **\n")
tn.write(b"ip address 172.16.10.254 255.255.255.0\n")
tn.write(b"no shut\n")
tn.write(b"exit\n")
tn.write(b"router ospf 101\n")
tn.write(b"network 1.1.0.1 255.255.255.255 area 0 \n")
tn.write(b"network 1.1.1.1 255.255.255.255 area 0 \n")
tn.write(b"network 1.1.2.1 255.255.255.255 area 0 \n")
tn.write(b"network 192.16.10.0 0.0.0.255 area 0 \n")
tn.write(b"network 172.16.10.0 0.0.0.255 area 0 \n")
tn.write(b"end\n")
tn.write(b"exit\n")
print(tn.read_all().decode('ascii'))